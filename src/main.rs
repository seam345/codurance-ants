enum Colour {
    Black,
    White,
}

#[derive(PartialEq, Debug)]
enum Orientation {
    Up,
    Left,
    Right,
    Down,
}

fn main() {
    println!("Hello, world!");
}

fn rotate_ant(square_colour: Colour, current_orientation: Orientation) -> Orientation {
    match square_colour {
        Colour::Black => match current_orientation {
            Orientation::Up => Orientation::Left,
            Orientation::Right => Orientation::Up,
            Orientation::Down => todo!(),
            Orientation::Left => todo!(),
        },
        Colour::White => {
            match current_orientation {
                Orientation::Up => Orientation::Right,
                Orientation::Right => Orientation::Down,
                Orientation::Down => Orientation::Left,
                Orientation::Left => Orientation::Up,
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{rotate_ant, Colour, Orientation};

    #[test]
    fn rotate_on_white_from_up() {
        let new_orientation = rotate_ant(Colour::White, Orientation::Up);
        assert_eq!(new_orientation, Orientation::Right);
    }

    #[test]
    fn rotate_on_white_from_right() {
        let new_orientation = rotate_ant(Colour::White, Orientation::Right);
        assert_eq!(new_orientation, Orientation::Down);
    }

    #[test]
    fn rotate_on_white_from_down() {
        let new_orientation = rotate_ant(Colour::White, Orientation::Down);
        assert_eq!(new_orientation, Orientation::Left);
    }

    #[test]
    fn rotate_on_white_from_left() {
        let new_orientation = rotate_ant(Colour::White, Orientation::Left);
        assert_eq!(new_orientation, Orientation::Up);
    }

    #[test]
    fn rotate_on_black_from_up() {
        let new_orientation = rotate_ant(Colour::Black, Orientation::Up);
        assert_eq!(new_orientation, Orientation::Left);
    }

    #[test]
    fn rotate_on_black_from_right() {
        let new_orientation = rotate_ant(Colour::Black, Orientation::Right);
        assert_eq!(new_orientation, Orientation::Up);
    }
}

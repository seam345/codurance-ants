## Learning todo!()
This was a quick kata to learn as much as I can about Rust [todo!()](https://doc.rust-lang.org/std/macro.todo.html)
macro, and how it would work with TDD (Test driven development). There are various `git diff` commands to show the
exact things I added/committed to git over the various adventures. It should also be possible to `git checkout <tag>`
and run `cargo test` to get the same results I did. Admittedly I forgot to commit `Cargo.lock` till a bit later than I
should however should be ok for such a simple program.

## The first test case 
```rust
#[test]
fn rotate_on_white_from_up() {
    let new_orientation = rotate_ant(Colour::White, Orientation::Up);
    assert_eq!(new_orientation, Orientation::Right);
}
```
With rusts strict type system todo didn't help me make very minimalist code here I have to work out what types the
arguments to rotate_ant function was going to be, this led down to quite a lot of thinking about the test and the code
for the test to compile. 
`cargo test` message was kinda nice though,
```shell
<snip>
---- tests::rotate_on_white_from_up stdout ----
thread 'tests::rotate_on_white_from_up' panicked at 'not yet implemented', src/main.rs:19:5
<snip>
```
but all this code was added to make it compile
```rust
enum Colour {
    Black,
    White,
}

#[derive(PartialEq, Debug)]
enum Orientation {
    Up,
    Left,
    Right,
    Down,
}


fn rotate_ant(square_colour: Colour, current_orientation: Orientation) -> Orientation {
    todo!();
}
```
you can see additions for first test with `git log first-test^ first-test`

### Writing functionality
`todo!()` did save us from having to think of anything to put in rotate ant, however as TDD the idea is to implement as
little code as possible to make the test pass I just replaced the `todo()!` with `Orientation::Right` felt like quite
the cop out but the test passed `git diff first-test-pass^ first-test-pass`.

## The second test case
```rust
#[test]
fn rotate_on_white_from_right() {
    let new_orientation = rotate_ant(Colour::White, Orientation::Right);
    assert_eq!(new_orientation, Orientation::Down);
}
```
Thankfully this test required no additional code for it to compile so it was a very simple addition `git diff second-test^ second-test`.
resulting in `cargo test` passing rotate_on_white_from_up and failing rotate_on_white_from_right,
```shell
<snip>

thread 'tests::rotate_on_white_from_right' panicked at 'assertion failed: `(left == right)`
  left: `Left`,
 right: `Down`', src/main.rs:40:9
 
<snip>
```

### Writing functionality
Initially riding the low of how little `todo()!` helped me before I wrote up the following for the test to pass
```rust
fn rotate_ant(square_colour: Colour, current_orientation: Orientation) -> Orientation {
    match current_orientation  {
        Orientation::Up => Orientation::Right,
        Orientation::Right => Orientation::Down,
        Orientation::Down => Orientation::Left,
        Orientation::Left => Orientation::Up,
    }
}
```
thinking all match arms had todo something so I have to add the other Orientations in, then todo really shined for me I
realised I can do this
```rust
fn rotate_ant(square_colour: Colour, current_orientation: Orientation) -> Orientation {
    match current_orientation  {
        Orientation::Up => Orientation::Right,
        Orientation::Right => Orientation::Down,
        Orientation::Down => todo!(),
        Orientation::Left => todo!(),
    }
}
```
Now admittedly that's not much of a difference but if each march arm was a whole complicated function that would be a lot
of time saved! And allowed us to focus just on the code we needed to write to pass the test.

the bellow is also possible, however my IDE (and most others) auto completed the match arms it would mean more effort todo
```rust
fn rotate_ant(square_colour: Colour, current_orientation: Orientation) -> Orientation {
    match current_orientation  {
        Orientation::Up => Orientation::Right,
        Orientation::Right => Orientation::Down,
        _ => todo!(),
    }
}
```

`cargo test` passes again :D `git diff second-test-pass^ second-test-pass`

## Third test case 
```rust
#[test]
fn rotate_on_white_from_down() {
    let new_orientation = rotate_ant(Colour::White, Orientation::Down);
    assert_eq!(new_orientation, Orientation::Left);
}
```

Aaaaa this is great we get a nice error just for this new test
```shell
running 3 tests
test tests::rotate_on_white_from_right ... ok
test tests::rotate_on_white_from_up ... ok
test tests::rotate_on_white_from_down ... FAILED

failures:

---- tests::rotate_on_white_from_down stdout ----
thread 'tests::rotate_on_white_from_down' panicked at 'not yet implemented', src/main.rs:22:30
<snip>
```
and it's the great "not yet implemented" perfect for TDD `git diff third-test^ third-test`

